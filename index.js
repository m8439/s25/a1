//2
db.fruits.aggregate([
		{$match: {"onSale":true }},
		{$count: "frutisOnSale"}
	])
//3 for more than 20 stocks
db.fruits.aggregate([
		{$match: {"stock":{$gt:20} }}, //or{$match: {"stock":{$gte:20} }}, if same as output
		{$count: "enoughStock"}
	])
//4
db.fruits.aggregate([
		{$match: {"onSale": true}},

		{$group: {_id: "$_id", average:{$avg:"$price"}}}
	])
db.fruits.aggregate([
		{$match: {"onSale": true}},

		{$group: {_id: null, average:{$avg:"$price"}}}
	])

//5
db.fruits.aggregate([
		{$match: {"onSale": true}},
		{$group: {_id: "$_id", max_price:{$max:"$price"}}}
	])
db.fruits.aggregate([
		{$match: {"onSale": true}},
		{$group: {_id: null, max_price:{$max:"$price"}}}
	])

//6
db.fruits.aggregate([
		{$match: {"onSale": true}},
		{$group: {_id: "$_id", min_price:{$min:"$price"}}}
	])
db.fruits.aggregate([
		{$match: {"onSale": true}},
		{$group: {_id: null, min_price:{$min:"$price"}}}
	])